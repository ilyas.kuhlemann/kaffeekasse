from pathlib import Path
import unittest
from unittest.mock import MagicMock, call, patch

import polars as pl

from kaffeekasse.data_reader import DataReader


class TestDataReader(unittest.TestCase):
    mock_polars_data_frame_1: pl.DataFrame
    mock_polars_data_frame_2: pl.DataFrame
    mock_polars_data_frame_3: pl.DataFrame
    mock_coffee_consumption_table_class: MagicMock
    mock_payouts_table_class: MagicMock
    mock_expenditures_table_class: MagicMock
    mock_coffee_consumption_table: MagicMock
    mock_payouts_table: MagicMock
    mock_expenditures_table: MagicMock
    mock_coffee_consumption_path: Path
    mock_payouts_path: Path
    mock_expenditures_path: Path
    mock_data_dir: Path
    sut: DataReader

    @patch("kaffeekasse.data_reader.ExpendituresTable")
    @patch("kaffeekasse.data_reader.PayoutsTable")
    @patch("kaffeekasse.data_reader.CoffeeConsumptionTable")
    def setUp(
        self,
        mock_coffee_consumption_table_class: MagicMock,
        mock_payouts_table_class: MagicMock,
        mock_expenditures_table_class: MagicMock,
    ) -> None:
        self.mock_coffee_consumption_table = MagicMock()
        self.mock_coffee_consumption_table_class = mock_coffee_consumption_table_class
        self.mock_coffee_consumption_table_class.return_value = (
            self.mock_coffee_consumption_table
        )

        self.mock_payouts_table = MagicMock()
        self.mock_payouts_table_class = mock_payouts_table_class
        self.mock_payouts_table_class.return_value = self.mock_payouts_table

        self.mock_expenditures_table = MagicMock()
        self.mock_expenditures_table_class = mock_expenditures_table_class
        self.mock_expenditures_table_class.return_value = (
            self.mock_expenditures_table
        )


        mock_table_1_before_filtering = MagicMock()
        mock_table_2_before_filtering = MagicMock()
        mock_table_3_before_filtering = MagicMock()
        self.mock_polars_data_frame_1 = MagicMock()
        self.mock_polars_data_frame_2 = MagicMock()
        self.mock_polars_data_frame_3 = MagicMock()
        mock_table_1_before_filtering.filter.return_value = self.mock_polars_data_frame_1
        mock_table_2_before_filtering.filter.return_value = self.mock_polars_data_frame_2
        mock_table_3_before_filtering.filter.return_value = self.mock_polars_data_frame_3
        pl.read_csv = MagicMock(
            side_effect=[
                mock_table_1_before_filtering,
                mock_table_2_before_filtering,
                mock_table_3_before_filtering
            ]
        )

        self.mock_data_dir = MagicMock()
        self.mock_coffee_consumption_path = MagicMock()
        self.mock_payouts_path = MagicMock()
        self.mock_expenditures_path = MagicMock()
        self.mock_data_dir.__truediv__ = MagicMock(
            side_effect=[
                self.mock_coffee_consumption_path,
                self.mock_payouts_path,
                self.mock_expenditures_path,
            ]
        )
        self.sut = DataReader(self.mock_data_dir, take_old_values_into_account_even_if_they_are_invalid_according_to_the_current_table_format=True)

    def test_that_sut_correctly_called_the_data_dir_path_to_get_the_path_to_the_coffee_consumption_file(
        self,
    ):
        expected_call = call("coffee_consumption.csv")
        self.assertIn(expected_call, self.mock_data_dir.__truediv__.mock_calls)

    def test_that_sut_correctly_called_polars_read_csv_to_read_the_coffee_consumption_file(
        self,
    ):
        expected_call = call(self.mock_coffee_consumption_path, quote_char="'")
        self.assertIn(expected_call, pl.read_csv.mock_calls)

    def test_that_sut_correctly_created_a_coffee_consumption_table_with_the_data_frame(
        self,
    ):
        self.mock_coffee_consumption_table_class.assert_called_once_with(
            self.mock_polars_data_frame_1
        )

    def test_that_get_coffee_consumption_table_returns_the_coffee_consumption_table(
        self,
    ):
        self.assertIs(
            self.mock_coffee_consumption_table, self.sut.get_coffee_consumption_table()
        )

    def test_that_sut_correctly_called_the_data_dir_path_to_get_the_path_to_the_payouts_file(
        self,
    ):
        expected_call = call("payouts.csv")
        self.assertIn(expected_call, self.mock_data_dir.__truediv__.mock_calls)

    def test_that_sut_correctly_called_polars_read_csv_to_read_the_payouts_file(self):
        expected_call = call(self.mock_payouts_path, quote_char="'")
        self.assertIn(expected_call, pl.read_csv.mock_calls)

    def test_that_sut_correctly_created_a_payouts_table_with_the_data_frame(self):
        self.mock_payouts_table_class.assert_called_once_with(
            self.mock_polars_data_frame_2
        )

    def test_that_get_payouts_table_returns_the_payouts_table(self):
        self.assertIs(self.mock_payouts_table, self.sut.get_payouts_table())

    def test_that_sut_correctly_called_the_data_dir_path_to_get_the_path_to_the_expenditures_file(
        self,
    ):
        expected_call = call("expenditures.csv")
        self.assertIn(expected_call, self.mock_data_dir.__truediv__.mock_calls)

    def test_that_sut_correctly_called_polars_read_csv_to_read_the_expenditures_file(
        self,
    ):
        expected_call = call(self.mock_expenditures_path, quote_char="'")
        self.assertIn(expected_call, pl.read_csv.mock_calls)

    def test_that_sut_correctly_created_an_expenditures_table_with_the_data_frame(self):
        self.mock_expenditures_table_class.assert_called_once_with(
            self.mock_polars_data_frame_3
        )

    def test_that_get_expenditures_table_returns_the_expenditures_table(self):
        self.assertIs(self.mock_expenditures_table, self.sut.get_expenditures_table())
