from datetime import date
import unittest
from unittest.mock import MagicMock

import polars as pl

from kaffeekasse.analyser import Analyser
from kaffeekasse.calculator.balance_calculator import BalanceCalculator
from kaffeekasse.calculator.expenditures_calculator import ExpendituresCalculator
from kaffeekasse.data_reader import DataReader
from kaffeekasse.tables.coffee_consumption_table import CoffeeConsumptionTable
from kaffeekasse.tables.expenditures_table import ExpendituresTable
from kaffeekasse.tables.payouts_table import PayoutsTable



class TestAnalyser(unittest.TestCase):
    payouts: pl.Series
    number_of_cups: pl.Series
    balance: pl.Series
    date_of_last_coffee_consumption_entry: date

    mock_payouts_table: PayoutsTable
    mock_coffee_consumption_table: CoffeeConsumptionTable
    mock_expenditures_table: ExpendituresTable
    mock_expenditures_calculator: ExpendituresCalculator
    mock_balance_calculator: BalanceCalculator
    mock_reader: DataReader
    sut: Analyser

    def setUp(self) -> None:
        self.date_of_last_coffee_consumption_entry = date(2022, 2, 22)
        self.mock_payouts_table = MagicMock()
        self.mock_coffee_consumption_table = MagicMock()
        self.mock_coffee_consumption_table.get_date_of_latest_entry.return_value = self.date_of_last_coffee_consumption_entry
        self.mock_expenditures_table = MagicMock()
        self._setup_table_return_data()
        self.mock_reader = MagicMock()
        self.mock_reader.get_coffee_consumption_table.return_value = (
            self.mock_coffee_consumption_table
        )
        self.mock_reader.get_payouts_table.return_value = self.mock_payouts_table
        self.mock_reader.get_expenditures_table.return_value = (
            self.mock_expenditures_table
        )
        self.mock_expenditures_calculator = MagicMock()
        self.mock_balance_calculator = MagicMock()
        self.sut = Analyser(
            self.mock_reader,
            self.mock_expenditures_calculator,
            self.mock_balance_calculator
        )

    def _setup_table_return_data(self) -> None:
        self.number_of_cups = pl.Series(values=[3000, 3010])
        self.payouts = pl.Series(values=[5.5, 4.5, 10.0])
        self.balance = pl.Series(values=[14.5, 44.5])
        self.mock_payouts_table.get_payout_amounts.return_value = self.payouts
        self.mock_coffee_consumption_table.get_number_of_coffee_cups.return_value = (
            self.number_of_cups
        )
        self.mock_coffee_consumption_table.get_kaffeekasse_balance.return_value = (
            self.balance
        )

    def test_that_the_coffee_consumption_table_was_retrieved_from_the_reader_during_init(
        self,
    ):
        self.mock_reader.get_coffee_consumption_table.assert_called_once_with()

    def test_that_the_payouts_table_was_retrieved_from_the_reader_during_init(self):
        self.mock_reader.get_payouts_table.assert_called_once_with()

    def test_that_get_average_price_payed_per_cup_gets_the_number_of_cups_from_the_coffee_consumption_table(
        self,
    ):
        self.sut.get_average_price_payed_per_cup()
        self.mock_coffee_consumption_table.get_number_of_coffee_cups.assert_called_once_with()

    def test_that_get_average_price_payed_per_cup_gets_the_kaffeekasse_balance_from_the_consumption_table(
        self,
    ):
        self.sut.get_average_price_payed_per_cup()
        self.mock_coffee_consumption_table.get_kaffeekasse_balance.assert_called_once_with()

    def test_that_get_average_price_payed_per_cup_gets_the_payout_amounts_from_the_payouts_table(
        self,
    ):
        self.sut.get_average_price_payed_per_cup()
        self.mock_payouts_table.get_payout_amounts.assert_called_once_with(self.date_of_last_coffee_consumption_entry)

    def test_that_average_price_payed_per_cup_is_computed_correctly_from_the_number_of_cups_and_the_balance_and_the_payouts_retrieved_from_the_tables(
        self,
    ):
        n_cups: int = self.number_of_cups[-1] - self.number_of_cups[0]
        total_sales_volume = (
            self.balance[-1] - self.balance[0]) + self.payouts.sum()
        expected_average_price_payed_per_cup = total_sales_volume / n_cups
        self.assertAlmostEqual(
            expected_average_price_payed_per_cup,
            self.sut.get_average_price_payed_per_cup().price_payed_per_cup,
        )

    def test_that_get_average_cost_per_cup_correctly_calls_the_expenditure_calculator_with_the_expenditures_table_to_compute_the_average_cost_per_kg_of_beans(
        self,
    ):
        self.sut.get_average_cost_per_cup()
        self.mock_expenditures_calculator.compute_average_cost_per_kg_of_beans.assert_called_once_with(
            self.mock_expenditures_table
        )

    def test_that_get_average_cost_per_cup_correctly_calls_the_expenditures_calculator_with_the_expenditures_table_to_compute_the_average_cost_per_liter_of_milk(
        self,
    ):
        self.sut.get_average_cost_per_cup()
        self.mock_expenditures_calculator.compute_average_cost_per_liter_of_milk.assert_called_once_with(
            self.mock_expenditures_table
        )

    def test_that_get_average_cost_per_cup_correctly_calls_the_consumption_table_to_compute_the_estimated_total_bean_consumption(
        self,
    ):
        self.sut.get_average_cost_per_cup()
        self.mock_coffee_consumption_table.get_beans_in_stock.assert_called_once_with()

    def test_that_get_average_cost_per_cup_correctly_calls_the_consumption_table_to_compute_the_estimated_total_milk_consumption(
        self,
    ):
        self.sut.get_average_cost_per_cup()
        self.mock_coffee_consumption_table.get_milk_in_stock.assert_called_once_with()

    def test_that_get_average_cost_per_cup_calls_the_expenditures_table_to_read_the_purchases_of_coffee(self):
        self.sut.get_average_cost_per_cup()
        self.mock_expenditures_table.get_coffee_expenditures.assert_called_once_with(self.date_of_last_coffee_consumption_entry)

    def test_that_get_average_cost_per_cup_calls_the_expenditures_table_to_read_the_purchases_of_milk(self):
        self.sut.get_average_cost_per_cup()
        self.mock_expenditures_table.get_milk_expenditures.assert_called_once_with(self.date_of_last_coffee_consumption_entry)

    def test_that_average_cost_per_cup_is_computed_correctly_from_the_values_retrieved_from_the_tables(
        self,
    ):
        average_cost_per_kg_of_beans: float = 18.2
        average_cost_per_liter_of_milk: float = 1.42

        beans_in_stock: pl.Series = pl.Series(values=[0, 1, 0.5, 2])
        milk_in_stock: pl.Series = pl.Series(values=[1, 2, 2, 1])

        beans_bought: pl.DataFrame = pl.DataFrame(
            data={"amount": [1.0, 0.25, 2.0]})
        milk_bought: pl.DataFrame = pl.DataFrame(
            data={"amount": [1.0, 2.0, 2.0]})

        self.mock_expenditures_calculator.compute_average_cost_per_kg_of_beans.return_value = average_cost_per_kg_of_beans
        self.mock_expenditures_calculator.compute_average_cost_per_liter_of_milk.return_value = average_cost_per_liter_of_milk
        self.mock_coffee_consumption_table.get_beans_in_stock.return_value = beans_in_stock
        self.mock_coffee_consumption_table.get_milk_in_stock.return_value = milk_in_stock
        self.mock_expenditures_table.get_coffee_expenditures.return_value = beans_bought
        self.mock_expenditures_table.get_milk_expenditures.return_value = milk_bought

        estimated_total_bean_consumption_in_kg: float = beans_bought["amount"].sum(
        ) - beans_in_stock[-1]
        estimated_total_milk_consumption_in_liter: float = milk_bought["amount"].sum(
        ) - milk_in_stock[-1]

        n_cups: int = self.number_of_cups[-1] - self.number_of_cups[0]

        average_bean_consumption_kg_per_cup: float = (
            estimated_total_bean_consumption_in_kg / n_cups
        )
        average_milk_consumption_liter_per_cup: float = (
            estimated_total_milk_consumption_in_liter / n_cups
        )
        expected_average_cost_per_cup: float = (
            average_bean_consumption_kg_per_cup * average_cost_per_kg_of_beans
        ) + (average_milk_consumption_liter_per_cup * average_cost_per_liter_of_milk)
        self.assertAlmostEqual(
            expected_average_cost_per_cup, self.sut.get_average_cost_per_cup().cost_per_cup
        )

    def test_that_get_balance_of_person_correctly_calls_the_balance_calculator_with_given_name_and_correct_tables(self):
        self.sut.get_balance_of_person("luke")
        self.mock_balance_calculator.calculate_balance_of_person.assert_called_once_with(
            "luke",
            self.mock_payouts_table,
            self.mock_expenditures_table
        )
    