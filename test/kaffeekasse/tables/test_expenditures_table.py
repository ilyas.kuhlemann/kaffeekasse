from datetime import date
import unittest

import polars as pl

from kaffeekasse.tables.expenditures_table import ExpendituresTable


class TestExpendituresTable(unittest.TestCase):
    raw_table: pl.DataFrame
    sut: ExpendituresTable

    def setUp(self) -> None:
        self.raw_table = pl.DataFrame(
            data=[
                pl.Series(
                    name="date",
                    values=["2024-01-01", "2024-02-01", "2024-03-01", "2024-04-01", "2024-05-01"]
                ),
                pl.Series(
                    name="expenditure",
                    values=[11, 21, 22, 12, 13]
                ),
                pl.Series(
                    name="product",
                    values=["coffee", "milk", "milk", "coffee", "coffee"],
                ),
                pl.Series(
                    name="amount",
                    values=["0.5 kg", "5 l", "3.5 l", "1.2 kg", "7 kg"]
                ),
                pl.Series(
                    name="who",
                    values=["ilyas", "ilyas", "ilyas", "arnold", "arnold"]
                )
            ]
        )
        self.sut = ExpendituresTable(self.raw_table)

    def test_that_get_coffee_expenditures_returns_a_dataframe_with_two_columns_named_amount_and_expenditure(self):
        df: pl.DataFrame = self.sut.get_coffee_expenditures()
        self.assertEqual(2, len(df.columns))
        self.assertIn("amount", df)
        self.assertIn("expenditure", df)

    def test_that_get_coffee_expenditure_returns_a_dataframe_with_expected_expenditure_column_from_raw_input_table(self):
        df: pl.DataFrame = self.sut.get_coffee_expenditures()
        self.assertEqual(df["expenditure"].to_list(), [11, 12, 13])

    def test_that_get_coffee_expenditure_returns_a_dataframe_with_expected_amount_column_from_raw_input_table(self):
        df: pl.DataFrame = self.sut.get_coffee_expenditures()
        self.assertEqual(df["amount"].to_list(), [0.5, 1.2, 7])

    def test_that_get_milk_expenditures_returns_a_dataframe_with_two_columns_named_amount_and_expenditure(self):
        df: pl.DataFrame = self.sut.get_milk_expenditures()
        self.assertEqual(2, len(df.columns))
        self.assertIn("amount", df)
        self.assertIn("expenditure", df)

    def test_that_get_milk_expenditure_returns_a_dataframe_with_expected_expenditure_column_from_raw_input_table(self):
        df: pl.DataFrame = self.sut.get_milk_expenditures()
        self.assertEqual(df["expenditure"].to_list(), [21, 22])

    def test_that_get_milk_expenditure_returns_a_dataframe_with_expected_amount_column_from_raw_input_table(self):
        df: pl.DataFrame = self.sut.get_milk_expenditures()
        self.assertEqual(df["amount"].to_list(), [5, 3.5])

    def test_that_get_expenditures_of_person_correctly_selects_the_columns_with_matching_name(self):
        df: pl.DataFrame = self.sut.get_expenditures_of_person("ilyas")
        self.assertEqual(df["expenditure"].to_list(), [11, 21, 22])

    def test_that_get_coffee_expenditures_correctly_neglects_the_3rd_coffee_entry_if_the_date_is_later_than_the_given_until_date_argument(self):
        df: pl.DataFrame = self.sut.get_coffee_expenditures(until_date=date(2024, 4, 1))
        self.assertEqual(df["amount"].to_list(), [0.5, 1.2])

    def test_that_get_milk_expenditures_correctly_neglects_the_2nd_milk_entry_if_the_date_is_later_than_the_given_until_date_argument(self):
        df: pl.DataFrame = self.sut.get_milk_expenditures(until_date=date(2024, 2, 28))
        self.assertEqual(df["amount"].to_list(), [5])