

from datetime import date
import unittest
from unittest.mock import MagicMock, call

import polars as pl

from kaffeekasse.tables.coffee_consumption_table import CoffeeConsumptionTable


class TestCoffeeConsumptionTable(unittest.TestCase):

    mock_raw_series: pl.Series
    mock_raw_table: pl.DataFrame
    sut: CoffeeConsumptionTable

    def setUp(self) -> None:
        self.mock_raw_table = MagicMock()
        self.mock_raw_series = MagicMock()
        self.mock_raw_table.__getitem__.return_value = self.mock_raw_series

        self.sut = CoffeeConsumptionTable(self.mock_raw_table)

    def test_that_get_kaffeekasse_balance_correctly_calls_the_raw_table_to_get_the_kaffeekasse_balance_colum_as_series(self) -> None:
        self.sut.get_kaffeekasse_balance()
        self.mock_raw_table.__getitem__.assert_called_once_with(
            "kaffeekasse balance / €"
        )

    def test_that_get_number_of_cups_correctly_calls_the_raw_table_to_get_the_total_number_of_cups_colum_and_the_number_of_water_cups_as_series(self) -> None:
        self.sut.get_number_of_coffee_cups()
        self.mock_raw_table.__getitem__.assert_has_calls([
            call(self.sut.COLUMN_TOTAL_CUPS),
            call(self.sut.COLUMN_WATER_CUPS)
        ])

    def test_that_get_beans_in_stock_calls_the_raw_table_to_get_the_number_of_opened_bean_packs(self):
        self.sut.get_beans_in_stock()
        self.mock_raw_table.__getitem__.assert_called_once_with(
            "1kg coffee packs in stock"
        )

    def test_that_get_milk_in_stock_calls_the_raw_table_to_get_the_number_of_opened_milk_packs(self):
        self.sut.get_milk_in_stock()
        self.mock_raw_table.__getitem__.assert_called_once_with(
            "1l milk packs in stock"
        )

    def test_that_get_date_of_latest_entry_correctly_reads_the_date_column(self):
        self.sut.get_date_of_latest_entry()
        self.mock_raw_table.__getitem__.assert_called_once_with(
            "date"
        )

    def test_that_get_date_of_latest_entry_correctly_converts_the_date_column_from_str_to_date(self):
        self.sut.get_date_of_latest_entry()
        self.mock_raw_series.str.to_date.assert_called_once_with()

    def test_that_get_date_of_latest_entry_correctly_returns_the_last_entry_from_the_date_column_as_datetime(self):
        self.mock_raw_series.str.to_date.return_value = pl.Series(
            ["2021-12-09", "2022-01-01", "2025-08-11"]).str.to_date()
        expected_date = date(2025, 8, 11)
        self.assertEqual(expected_date, self.sut.get_date_of_latest_entry())
