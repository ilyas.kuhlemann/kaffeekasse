

from datetime import date
import unittest
from unittest.mock import MagicMock

import polars as pl

from kaffeekasse.tables.payouts_table import PayoutsTable


class TestPayoutsTable(unittest.TestCase):

    mock_raw_table: pl.DataFrame
    sut: PayoutsTable

    def setUp(self) -> None:
        self.mock_raw_table = MagicMock()
        self.sut = PayoutsTable(self.mock_raw_table)

    def test_that_get_payout_amounts_correctly_calls_the_raw_table_to_get_the_payout_amounts_colum_as_series(self):
        self.sut.get_payout_amounts()
        self.mock_raw_table.__getitem__.assert_called_once_with("amount/€")

    def test_that_get_payout_amounts_of_person_correctly_filters_the_raw_table_for_payouys_by_given_person(self):
        person_name = "ewald"
        other_person_name = "juergen"
        table = pl.DataFrame(
            [
                pl.Series(self.sut.AMOUNT_COL, [1, 2, 3, 4, 5]),
                pl.Series(self.sut.PERSON_NAME_COL, [
                    person_name,
                    other_person_name,
                    person_name,
                    other_person_name,
                    person_name
                ])
            ]
        )
        self.sut = PayoutsTable(table)
        payouts_of_person = self.sut.get_payout_amounts_of_person(person_name)
        self.assertEqual(
            [1, 3, 5],
            payouts_of_person.to_list()
        )

    def test_that_get_payout_amounts_returns_two_payout_amount_entries_if_the_full_table_has_3_entries_but_the_last_entry_is_later_than_the_given_until_date(self):
        table = pl.DataFrame(
            [
                pl.Series(self.sut.AMOUNT_COL, [1, 2, 3]),
                pl.Series(self.sut.DATE_COL, ['2024-01-01', '2024-02-01', '2024-03-01'])
            ]
        )
        self.sut = PayoutsTable(table)
        amounts = self.sut.get_payout_amounts(until_date=date(2024, 2, 28))
        self.assertEqual(amounts.to_list(), [1, 2])
