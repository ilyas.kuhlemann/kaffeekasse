import unittest
from unittest.mock import MagicMock

import polars as pl

from kaffeekasse.calculator.expenditures_calculator import ExpendituresCalculatorImpl
from kaffeekasse.tables.expenditures_table import ExpendituresTable


class TestExpendituresCalculatorImpl(unittest.TestCase):
    milk_expenditures: pl.DataFrame
    coffee_expenditures: pl.DataFrame
    mock_table: ExpendituresTable
    sut: ExpendituresCalculatorImpl

    def setUp(self) -> None:
        coffee_purchase_amounts = pl.Series(name="amount", values=[1, 0.75, 2])
        coffee_purchase_expenditure = pl.Series(
            name="expenditure", values=[12.0, 13.5, 35.5]
        )
        self.coffee_expenditures = pl.DataFrame(
            data=[coffee_purchase_expenditure, coffee_purchase_amounts]
        )
        milk_purchase_amounts = pl.Series(name="amount", values=[1, 2, 2])
        milk_purchase_expenditure = pl.Series(
            name="expenditure", values=[3.0, 4.5, 5.5]
        )
        self.milk_expenditures = pl.DataFrame(
            data=[milk_purchase_expenditure, milk_purchase_amounts]
        )

        self.mock_table = MagicMock()
        self.mock_table.get_coffee_expenditures.return_value = self.coffee_expenditures
        self.mock_table.get_milk_expenditures.return_value = self.milk_expenditures
        self.sut = ExpendituresCalculatorImpl()

    def test_that_compute_average_cost_per_kg_of_beans_correctly_calls_the_table_to_get_the_coffee_expenditures(
        self,
    ):
        self.sut.compute_average_cost_per_kg_of_beans(self.mock_table)
        self.mock_table.get_coffee_expenditures.assert_called_once_with()

    def test_that_compute_average_cost_per_kg_of_beans_correctly_computes_the_average_cost_from_The_given_table(
        self,
    ):
        total_purchased_amount = self.coffee_expenditures["amount"].sum()
        total_cost = self.coffee_expenditures["expenditure"].sum()
        cost_per_kg = total_cost / total_purchased_amount
        self.assertAlmostEqual(
            cost_per_kg, self.sut.compute_average_cost_per_kg_of_beans(self.mock_table)
        )

    def test_that_compute_average_cost_per_liter_of_milk_correctly_calls_the_table_to_get_the_milk_expenditures(
        self,
    ):
        self.sut.compute_average_cost_per_liter_of_milk(self.mock_table)
        self.mock_table.get_milk_expenditures.assert_called_once_with()

    def test_that_compute_average_cost_per_liter_of_milk_correctly_computes_the_average_cost_from_The_given_table(
        self,
    ):
        total_purchased_amount = self.milk_expenditures["amount"].sum()
        total_cost = self.milk_expenditures["expenditure"].sum()
        cost_per_kg = total_cost / total_purchased_amount
        self.assertAlmostEqual(
            cost_per_kg, self.sut.compute_average_cost_per_liter_of_milk(self.mock_table)
        )
