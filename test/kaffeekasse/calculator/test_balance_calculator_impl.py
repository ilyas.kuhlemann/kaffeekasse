

from typing import Any
import unittest
from unittest.mock import MagicMock

import polars as pl

from kaffeekasse.calculator.balance_calculator import BalanceCalculatorImpl
from kaffeekasse.tables.expenditures_table import ExpendituresTable
from kaffeekasse.tables.payouts_table import PayoutsTable


class TestBalanceCalculatorImpl(unittest.TestCase):

    sut: BalanceCalculatorImpl
    mock_expenditures_table: ExpendituresTable
    mock_payouts_table: PayoutsTable

    def setUp(self) -> None:
        self.mock_expenditures_table = MagicMock()
        self.mock_payouts_table = MagicMock()
        mock_payouts = MagicMock()
        mock_payouts.sum.return_value = 0
        self.mock_payouts_table.get_payout_amounts_of_person.return_value = mock_payouts
        self.sut = BalanceCalculatorImpl()

    def test_that_calculate_balance_of_ilyas_correctly_calls_the_expenditures_table_to_get_the_expenditures_of_ilyas(self):
        name = "ilyas"
        self.sut.calculate_balance_of_person(
            name,
            self.mock_payouts_table,
            self.mock_expenditures_table
        )
        self.mock_expenditures_table.get_expenditures_of_person.assert_called_once_with(
            name)

    def test_that_calculate_balance_of_person_correctly_returns_the_sum_of_all_expenditures_if_payouts_is_zero(self):
        name = "ilyas"
        self.mock_expenditures_table.get_expenditures_of_person.return_value = self._create_expenditures_table_with_expenditure_amounts(
            {name: [1.5, 2, 3.25]}
        )
        balance: float = self.sut.calculate_balance_of_person(
            name,
            self.mock_payouts_table,
            self.mock_expenditures_table
        )
        expected_balance = 6.75
        self.assertEqual(
            expected_balance,
            balance
        )

    def test_that_calculate_balance_of_person_correctly_calls_the_payouts_table_to_get_the_payouts_of_the_given_person(self):
        person_name = "rené"
        payouts_of_person = pl.Series("amount/€", [13, 2.25])
        self.mock_payouts_table.get_payout_amounts_of_person.return_value = payouts_of_person
        self.mock_expenditures_table.get_expenditures_of_person.return_value = self._create_expenditures_table_with_expenditure_amounts(
            {
                person_name: [2.5, 3, 4.25],
            }
        )
        balance: float = self.sut.calculate_balance_of_person(
            person_name,
            self.mock_payouts_table,
            self.mock_expenditures_table
        )
        expected_balance = -5.5
        self.assertEqual(
            expected_balance,
            balance
        )

    def test_that_calculate_balance_of_person_correctly_returns_the_sum_of_all_expenditures_minus_the_sum_of_all_payouts(self):
        name = "ilyas"
        self.mock_expenditures_table.get_expenditures_of_person.return_value = self._create_expenditures_table_with_expenditure_amounts(
            {
                name: [2.5, 3, 4.25],
            }
        )
        balance: float = self.sut.calculate_balance_of_person(
            name,
            self.mock_payouts_table,
            self.mock_expenditures_table
        )
        expected_balance = 9.75
        self.assertEqual(
            expected_balance,
            balance
        )

    def _create_expenditures_table_with_expenditure_amounts(self, amounts_by_person: dict[str, list[float]]) -> pl.DataFrame:
        table_data: dict[str, list[Any]] = {
            "who": [],
            "expenditure": []
        }
        for name in amounts_by_person:
            amounts: list[float] = amounts_by_person[name]
            name_data: list[str] = [name]*len(amounts)
            table_data["who"].extend(name_data)
            table_data["expenditure"].extend(amounts)
        return pl.DataFrame(table_data)
