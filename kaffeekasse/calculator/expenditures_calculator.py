from abc import ABC, abstractmethod

import polars as pl

from kaffeekasse.tables.expenditures_table import ExpendituresTable



class ExpendituresCalculator(ABC):

    @abstractmethod
    def compute_average_cost_per_kg_of_beans(
        self,
        expenditures_table: ExpendituresTable
    ) -> float:
        pass

    @abstractmethod
    def compute_average_cost_per_liter_of_milk(
        self,
        expenditures_table: ExpendituresTable
    ) -> float:
        pass


class ExpendituresCalculatorImpl(ExpendituresCalculator):
    def compute_average_cost_per_kg_of_beans(
        self,
        expenditures_table: ExpendituresTable
    ) -> float:
        coffee_expenditures_table: pl.DataFrame = (
            expenditures_table.get_coffee_expenditures()
        )
        return self._compute_average_cost_per_unit(coffee_expenditures_table)

    def _compute_average_cost_per_unit(
        self,
        expenditures_dataframe: pl.DataFrame
    ) -> float:
        total_purchased_amount = expenditures_dataframe["amount"].sum()
        total_cost = expenditures_dataframe["expenditure"].sum()
        cost_per_kg = total_cost / total_purchased_amount
        return cost_per_kg

    def compute_average_cost_per_liter_of_milk(
        self, expenditures_table: ExpendituresTable
    ) -> float:
        milk_expenditures_table: pl.DataFrame = (
            expenditures_table.get_milk_expenditures()
        )
        return self._compute_average_cost_per_unit(milk_expenditures_table)
