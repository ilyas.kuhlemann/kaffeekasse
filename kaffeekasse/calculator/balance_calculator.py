from abc import ABC, abstractmethod

import polars as pl

from kaffeekasse.tables.expenditures_table import ExpendituresTable
from kaffeekasse.tables.payouts_table import PayoutsTable




class BalanceCalculator(ABC):

    @abstractmethod
    def calculate_balance_of_person(
        self,
        name: str,
        payouts_table: PayoutsTable,
        expenditures_table: ExpendituresTable
    ) -> float:
        pass


class BalanceCalculatorImpl(BalanceCalculator):

    def calculate_balance_of_person(
        self, 
        name: str, 
        payouts_table: PayoutsTable, 
        expenditures_table: ExpendituresTable
    ) -> float:
        expenditures: pl.DataFrame = expenditures_table.get_expenditures_of_person(name)
        payouts: pl.Series = payouts_table.get_payout_amounts_of_person(name)
        return expenditures["expenditure"].sum() - payouts.sum()
