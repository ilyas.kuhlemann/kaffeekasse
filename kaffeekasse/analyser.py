from datetime import date
import polars as pl
from kaffeekasse.analyser_models import CostPerCupResult, PricePayedPerCupResult
from kaffeekasse.calculator.balance_calculator import BalanceCalculator, BalanceCalculatorImpl
from kaffeekasse.calculator.expenditures_calculator import ExpendituresCalculator, ExpendituresCalculatorImpl
from kaffeekasse.data_reader import DataReader
from kaffeekasse.tables.coffee_consumption_table import CoffeeConsumptionTable
from kaffeekasse.tables.expenditures_table import ExpendituresTable
from kaffeekasse.tables.payouts_table import PayoutsTable


class Analyser:

    _AT_FIRST_RECORD = 0
    _AT_LATEST_RECORD = -1

    def __init__(
        self,
        data_reader: DataReader,
        expenditures_calculator: ExpendituresCalculator = ExpendituresCalculatorImpl(),
        balance_calculator: BalanceCalculator = BalanceCalculatorImpl(),
    ) -> None:
        self._coffee_consumption_table: CoffeeConsumptionTable = (
            data_reader.get_coffee_consumption_table()
        )
        self._payouts_table: PayoutsTable = data_reader.get_payouts_table()
        self._expenditures_table: ExpendituresTable = data_reader.get_expenditures_table()
        self._expenditures_calculator = expenditures_calculator
        self._balance_calculator = balance_calculator

    def get_average_price_payed_per_cup(self) -> PricePayedPerCupResult:
        number_of_cups: pl.Series = self._coffee_consumption_table.get_number_of_coffee_cups()
        balance: pl.Series = self._coffee_consumption_table.get_kaffeekasse_balance()
        date_of_latest_coffee_consumption_table_entry: date = self._coffee_consumption_table.get_date_of_latest_entry()
        payouts: pl.Series = self._payouts_table.get_payout_amounts(date_of_latest_coffee_consumption_table_entry)
        n_cups: int = (
            number_of_cups[self._AT_LATEST_RECORD] - number_of_cups[self._AT_FIRST_RECORD]
        )
        total_sales_volume = (
            balance[self._AT_LATEST_RECORD] - balance[self._AT_FIRST_RECORD]
        ) + payouts.sum()
        return PricePayedPerCupResult(
            total_sales_volume / n_cups,
            date_of_latest_coffee_consumption_table_entry
        )

    def get_average_cost_per_cup(self) -> CostPerCupResult:
        cost_of_beans = self._expenditures_calculator.compute_average_cost_per_kg_of_beans(self._expenditures_table)
        cost_of_milk = self._expenditures_calculator.compute_average_cost_per_liter_of_milk(self._expenditures_table)
        beans_in_stock: pl.Series = self._coffee_consumption_table.get_beans_in_stock()
        milk_in_stock: pl.Series = self._coffee_consumption_table.get_milk_in_stock()
        date_of_last_coffee_consumption_table_entry: date = self._coffee_consumption_table.get_date_of_latest_entry()
        beans_bought: pl.DataFrame = self._expenditures_table.get_coffee_expenditures(
            date_of_last_coffee_consumption_table_entry
        )
        milk_bought: pl.DataFrame = self._expenditures_table.get_milk_expenditures(
            date_of_last_coffee_consumption_table_entry
        )
        total_bean_consumption: float = beans_bought["amount"].sum() - beans_in_stock[-1]
        total_milk_consumption: float = milk_bought["amount"].sum() - milk_in_stock[-1]
        number_of_cups: pl.Series = self._coffee_consumption_table.get_number_of_coffee_cups()
        n_cups: int = (
            number_of_cups[self._AT_LATEST_RECORD] - number_of_cups[self._AT_FIRST_RECORD]
        )
        return CostPerCupResult(
            ((cost_of_beans * total_bean_consumption) + (cost_of_milk * total_milk_consumption)) / n_cups,
            date_of_latest_coffee_consumption_entry=date_of_last_coffee_consumption_table_entry
        )

    def get_balance_of_person(self, name: str) -> float:
        return self._balance_calculator.calculate_balance_of_person(
            name,
            self._payouts_table,
            self._expenditures_table
        )
