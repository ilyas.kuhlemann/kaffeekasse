from datetime import date

import polars as pl


class CoffeeConsumptionTable:

    COLUMN_TOTAL_CUPS = "number of total cups"
    COLUMN_WATER_CUPS = "number of hot water cups"

    def __init__(self, raw_table: pl.DataFrame) -> None:
        self._raw_table = raw_table

    def get_number_of_coffee_cups(self) -> pl.Series:
        number_of_coffee_cups = self._raw_table[self.COLUMN_TOTAL_CUPS] - self._raw_table[self.COLUMN_WATER_CUPS]
        return number_of_coffee_cups

    def get_kaffeekasse_balance(self) -> pl.Series:
        return self._raw_table["kaffeekasse balance / €"]

    def get_beans_in_stock(self) -> pl.Series:
        return self._raw_table["1kg coffee packs in stock"]

    def get_milk_in_stock(self) -> pl.Series:
        return self._raw_table["1l milk packs in stock"]

    def get_date_of_latest_entry(self) -> date:
        dates: pl.Series = self._raw_table["date"].str.to_date()
        return dates[-1]
