from datetime import date
import polars as pl


class PayoutsTable:

    DATE_COL = "date"
    AMOUNT_COL = "amount/€"
    PERSON_NAME_COL = "who"

    def __init__(self, raw_table: pl.DataFrame) -> None:
        self._raw_table = raw_table

    def get_payout_amounts(self, until_date: date | None = None) -> pl.Series:
        if until_date is None:
            table = self._raw_table
        else:
            table = self._raw_table.filter(pl.col(self.DATE_COL).str.to_date() <= until_date)
        return table[self.AMOUNT_COL]

    def get_payout_amounts_of_person(self, person_name: str) -> pl.Series:
        df = self._raw_table.filter(pl.col(self.PERSON_NAME_COL) == person_name)
        return df[self.AMOUNT_COL]
