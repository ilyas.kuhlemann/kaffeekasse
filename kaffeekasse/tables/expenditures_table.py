from datetime import date
from kaffeekasse.tables.table_models import Product


import polars as pl


from typing import Literal


class ExpendituresTable:

    def __init__(self, raw_table: pl.DataFrame) -> None:
        self._raw_table = raw_table

    def get_coffee_expenditures(self, until_date: date | None = None) -> pl.DataFrame:
        raw_table = self._raw_table
        if until_date is not None:
            raw_table = raw_table.filter(pl.col("date").str.to_date() <= until_date)
        return self._get_sanitized_float_expenditure_table_filtered_for_product(
            "coffee", raw_table
        )

    def _get_sanitized_float_expenditure_table_filtered_for_product(
        self,
        product: Literal["coffee", "milk"],
        raw_table: pl.DataFrame
    ) -> pl.DataFrame:
        filtered_expenditure: pl.DataFrame = raw_table.filter(
            pl.col("product") == product)
        return pl.DataFrame(data=[
            filtered_expenditure["expenditure"],
            filtered_expenditure["amount"].map_elements(
                lambda x: float(x[:-2]))
        ])

    def get_milk_expenditures(self, until_date: date | None = None) -> pl.DataFrame:
        raw_table = self._raw_table
        if until_date is not None:
            raw_table = raw_table.filter(pl.col("date").str.to_date() <= until_date)
        return self._get_sanitized_float_expenditure_table_filtered_for_product(
            "milk",
            raw_table
        )

    def get_expenditures_of_person(self, name: str) -> pl.DataFrame:
        return self._raw_table.filter(pl.col("who") == name)

    def get_product_purchases(self, product: Product) -> pl.DataFrame:
        return self._raw_table.filter(pl.col("product") == product).select(pl.col("date"), pl.col("amount"))
