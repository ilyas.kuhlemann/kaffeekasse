from datetime import date
from pathlib import Path

import polars as pl

from kaffeekasse.config import get_date_of_first_valid_entry
from kaffeekasse.tables.coffee_consumption_table import CoffeeConsumptionTable
from kaffeekasse.tables.expenditures_table import ExpendituresTable
from kaffeekasse.tables.payouts_table import PayoutsTable


class DataReader:
    def __init__(self, data_dir: Path, take_old_values_into_account_even_if_they_are_invalid_according_to_the_current_table_format: bool = False) -> None:
        self._data_dir: Path = data_dir
        if take_old_values_into_account_even_if_they_are_invalid_according_to_the_current_table_format:
            some_date_in_the_past_way_before_this_project_started = date(1999, 9, 9)
            date_of_first_valid_entry = some_date_in_the_past_way_before_this_project_started
        else:
            date_of_first_valid_entry: date = get_date_of_first_valid_entry()
        raw_consumption_table = pl.read_csv(
            self._data_dir / "coffee_consumption.csv", quote_char="'"
        ).filter(pl.col('date').str.to_date() >= date_of_first_valid_entry)
        raw_payouts_table = pl.read_csv(
            self._data_dir / "payouts.csv", quote_char="'"
        ).filter(pl.col('date').str.to_date() >= date_of_first_valid_entry)
        raw_expenditures_table = pl.read_csv(
            self._data_dir / "expenditures.csv",
            quote_char="'"
        ).filter(pl.col('date').str.to_date() >= date_of_first_valid_entry)

        self._coffee_consumption_table = CoffeeConsumptionTable(
            raw_consumption_table)
        self._payouts_table = PayoutsTable(raw_payouts_table)
        self._expenditures_table = ExpendituresTable(raw_expenditures_table)

    def get_coffee_consumption_table(self) -> CoffeeConsumptionTable:
        return self._coffee_consumption_table

    def get_payouts_table(self) -> PayoutsTable:
        return self._payouts_table

    def get_expenditures_table(self) -> ExpendituresTable:
        return self._expenditures_table
