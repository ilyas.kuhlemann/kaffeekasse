
from argparse import ArgumentParser
from kaffeekasse.paths import KAFFEEKASSE_DATA_DIR
from kaffeekasse.analyser import Analyser
from kaffeekasse.data_reader import DataReader

def main() -> None:
    parser = ArgumentParser()
    parser.add_argument("name")
    args = parser.parse_args()
    name: str = args.name
    data_reader = DataReader(
        KAFFEEKASSE_DATA_DIR, 
        take_old_values_into_account_even_if_they_are_invalid_according_to_the_current_table_format=True
    )
    balance: float = Analyser(data_reader).get_balance_of_person(name)
    print(f"Balance: {balance:.2f} €")

if __name__ == "__main__":
    main()