
from datetime import datetime
import polars as pl
import shutil

from kaffeekasse.paths import COFFEE_CONSUMPTION_FILE, KAFFEEKASSE_DATA_DIR
from kaffeekasse.tables import ExpendituresTable, Product
from kaffeekasse.data_reader import DataReader


def main() -> None:
    import sys
    print("Careful: Migration was already done! Will exit without doing anything.")
    sys.exit(1)
    backup_file = COFFEE_CONSUMPTION_FILE.parent / \
        "coffee_consumption.migration_000_backup.csv"
    shutil.copy(
        COFFEE_CONSUMPTION_FILE,
        backup_file
    )
    print(f"Copied {COFFEE_CONSUMPTION_FILE} to {backup_file}")

    table: pl.DataFrame = pl.read_csv(COFFEE_CONSUMPTION_FILE)
    expenditures: ExpendituresTable = DataReader(
        KAFFEEKASSE_DATA_DIR).get_expenditures_table()
    milk_purchases: pl.DataFrame = expenditures.get_product_purchases(
        product=Product.MILK
    )
    coffee_purchases: pl.DataFrame = expenditures.get_product_purchases(
        product=Product.COFFEE
    )

    milk_packs_in_stock_column: pl.Series = _create_milk_packs_in_stock_column(
        table, milk_purchases)
    coffe_packs_in_stock_column: pl.Series = _create_coffee_packs_in_stock_column(
        table, coffee_purchases)
    new_table = table.select(
        pl.col("date"),
        pl.col("'kaffeekasse balance / €'"),
        pl.col("'number of total cups'")
    ).with_columns(
        coffe_packs_in_stock_column,
        milk_packs_in_stock_column
    )

    print(new_table)
    print(f"Writing table to {COFFEE_CONSUMPTION_FILE}")
    new_table.write_csv(COFFEE_CONSUMPTION_FILE)


def _create_milk_packs_in_stock_column(
    old_coffee_consumption_table: pl.DataFrame,
    milk_purchases: pl.DataFrame
) -> pl.Series:
    milk_packs_in_stock: list[int] = []
    total_number_of_milk_packs_used_until_date = 0
    for row in old_coffee_consumption_table.iter_rows(named=True):
        date: datetime = _str_to_datetime(row["date"])
        number_of_packs_bought_until_date: int = int(_get_product_amount_bought_until_date(
            date,
            milk_purchases
        ))
        print(row)
        total_number_of_milk_packs_used_until_date += row["'number of 1l milk packages opened since last record'"]
        packs_in_stock = number_of_packs_bought_until_date - \
            total_number_of_milk_packs_used_until_date
        milk_packs_in_stock.append(packs_in_stock)
    return pl.Series("'1l milk packs in stock'", milk_packs_in_stock)


def _get_product_amount_bought_until_date(date_limit: datetime, product_purchases: pl.DataFrame) -> float:
    sum_packs = 0
    for row in product_purchases.iter_rows(named=True):
        date = _str_to_datetime(row['date'])
        if date > date_limit:
            return sum_packs
        sum_packs += _parse_amount_with_unit_str(row['amount'])
    return sum_packs


def _parse_amount_with_unit_str(amount_str_with_unit: str) -> float:
    return float(amount_str_with_unit.split()[0])


def _str_to_datetime(datestr: str) -> datetime:
    split_datestr = [int(v) for v in datestr.split('-')]
    return datetime(split_datestr[0], split_datestr[1], split_datestr[2])


def _create_coffee_packs_in_stock_column(
    old_coffee_consumption_table: pl.DataFrame,
    coffee_purchases: pl.DataFrame
) -> pl.Series:
    coffee_packs_in_stock: list[float] = []
    total_number_of_coffee_packs_used_until_date = 0.0
    for row in old_coffee_consumption_table.iter_rows(named=True):
        date: datetime = _str_to_datetime(row["date"])
        number_of_packs_bought_until_date: float = _get_product_amount_bought_until_date(
            date,
            coffee_purchases
        )
        print(row)
        total_number_of_coffee_packs_used_until_date += row["'number of 1kg coffee packs opened since last record'"]
        packs_in_stock = number_of_packs_bought_until_date - \
            total_number_of_coffee_packs_used_until_date
        coffee_packs_in_stock.append(packs_in_stock)
    return pl.Series("'1kg coffee packs in stock'", coffee_packs_in_stock)


if __name__ == "__main__":
    main()
