
from kaffeekasse.analyser_models import PricePayedPerCupResult
from kaffeekasse.paths import KAFFEEKASSE_DATA_DIR
from kaffeekasse.analyser import Analyser
from kaffeekasse.data_reader import DataReader


def main() -> None:
    data_reader = DataReader(KAFFEEKASSE_DATA_DIR)
    result: PricePayedPerCupResult = Analyser(data_reader).get_average_price_payed_per_cup()
    print(f"Date of latest entry in the coffee consumption table: {result.date_of_latest_coffee_consumption_entry}")
    print(f"Average price payed per cup: {result.price_payed_per_cup:.2f} €")


if __name__ == "__main__":
    main()
