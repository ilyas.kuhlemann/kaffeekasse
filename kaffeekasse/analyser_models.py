

from dataclasses import dataclass
from datetime import date



@dataclass
class CostPerCupResult:
    cost_per_cup: float
    date_of_latest_coffee_consumption_entry: date

@dataclass
class PricePayedPerCupResult:
    price_payed_per_cup: float
    date_of_latest_coffee_consumption_entry: date