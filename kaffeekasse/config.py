

from datetime import date
from os import getenv


def get_date_of_first_valid_entry() -> date:
    first_valid_date_str = getenv("KAFFEEKASSE_FIRST_VALID_ENTRY_DATE")
    if first_valid_date_str is None:
        raise KeyError(
            "Environment variable '$KAFFEEKASSE_FIRST_VALID_ENTRY_DATE' is not defined. "
            "This needs to be defined s.th. only valid entries are taken into account. "
            "The table format has changed during development, hence the old entries with missing data "
            "need to be ignored. "
            "Check the coffee_consumption.csv table for the first entry that has the full data, i.e. that is not missing data "
            "in the 'number of hot water cups' column. The date of that entry is what you are looking fore. Then define this "
            "date in the env var as 'yyyy-mm-dd', e.g. 'export KAFFEEKASSE_FIRST_VALID_ENTRY_DATE=2024-09-13'."
        )
    split_date = first_valid_date_str.split('-')
    return date(int(split_date[0]), int(split_date[1]), int(split_date[2]))
