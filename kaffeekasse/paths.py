
from os import getenv
from pathlib import Path

_data_dir = getenv("KAFFEEKASSE_DATA_DIR")
if _data_dir is None:
    raise KeyError("Environment variable 'KAFFEEKASSE_DATA_DIR' not defined!")
KAFFEEKASSE_DATA_DIR: Path = Path(_data_dir)
COFFEE_CONSUMPTION_FILE: Path = KAFFEEKASSE_DATA_DIR / "coffee_consumption.csv"